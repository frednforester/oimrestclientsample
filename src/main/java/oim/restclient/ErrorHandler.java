/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package oim.restclient;

import org.apache.log4j.Logger;

/**
 
 * @author Owner
 */
public class ErrorHandler {
    
    
    private static final Logger LOGGER = Logger.getLogger(ErrorHandler.class);
    
    public static void checkResponse(int respCode,String msg) throws Exception
    {
        if (respCode != 200)
        {
            String error = ErrorHandler.getErrorMessage(respCode, msg);
            if (error != null)
            {
                LOGGER.error("Error:" + error);
                throw new Exception(error);
            }
        }
    }
    public static String getErrorMessage(int respCode,String msg)
    {
        // server error
        if (respCode >= 500)
        {
            return msg;
        }
        
        // not found
        if (respCode >= 400)
        {
            return msg;
        }
        
        if (respCode >= 300)
        {
            return msg;
        }
        
        if (respCode >= 200)
        {
            return null;
        }
        
        if (respCode >= 100)
        {
            return msg;
        }
        return msg;
        
    }
    
}
