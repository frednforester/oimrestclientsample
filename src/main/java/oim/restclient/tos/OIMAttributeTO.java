/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oim.restclient.tos;

/**
 *
 * @author Owner
 */
public class OIMAttributeTO {
    
    private String name;
    private Object value;

    public OIMAttributeTO(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public OIMAttributeTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "OIMAttributeTO{" + "name=" + name + ", value=" + value + '}';
    }
    
    
}
