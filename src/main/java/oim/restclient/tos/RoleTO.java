/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oim.restclient.tos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Owner
 */
public class RoleTO {

    private String id;
    private String displayname;
    private String ownerlogin;
    private String rolekey;
    private List<OIMAttributeTO> fields = new ArrayList();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getOwnerlogin() {
        return ownerlogin;
    }

    public void setOwnerlogin(String ownerlogin) {
        this.ownerlogin = ownerlogin;
    }

    public String getRolekey() {
        return rolekey;
    }

    public void setRolekey(String rolekey) {
        this.rolekey = rolekey;
    }

    public List<OIMAttributeTO> getFields() {
        return fields;
    }

    public void setFields(List<OIMAttributeTO> fields) {
        this.fields = fields;
    }

    @Override
    public String toString() {
        return "RoleTO{" + "id=" + id + ", displayname=" + displayname + ", ownerlogin=" + ownerlogin + ", rolekey=" + rolekey + ", fields=" + fields + '}';
    }
    
    

}
