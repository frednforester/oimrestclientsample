/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oim.restclient.tos;

/**
 *
 * @author Owner
 */
public class ApplicationTO {
    
  private String id;
  private String name;
  private String dataSetName;
  private String description;
  private String displayName;
  private String itResourceKey;
  private String itResourceName;
  private String objectKey;
  private String objectName;
  private String type;
  private String isSoftDelete;
  private String parentKey;
  private String status;
  private String requestId;
  private String reason;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDataSetName() {
        return dataSetName;
    }

    public void setDataSetName(String dataSetName) {
        this.dataSetName = dataSetName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getItResourceKey() {
        return itResourceKey;
    }

    public void setItResourceKey(String itResourceKey) {
        this.itResourceKey = itResourceKey;
    }

    public String getItResourceName() {
        return itResourceName;
    }

    public void setItResourceName(String itResourceName) {
        this.itResourceName = itResourceName;
    }

    public String getObjectKey() {
        return objectKey;
    }

    public void setObjectKey(String objectKey) {
        this.objectKey = objectKey;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIsSoftDelete() {
        return isSoftDelete;
    }

    public void setIsSoftDelete(String isSoftDelete) {
        this.isSoftDelete = isSoftDelete;
    }

    public String getParentKey() {
        return parentKey;
    }

    public void setParentKey(String parentKey) {
        this.parentKey = parentKey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "ApplicationTO{" + "id=" + id + ", name=" + name + ", itResourceKey=" + itResourceKey + ", itResourceName=" + itResourceName + ", objectKey=" + objectKey + ", objectName=" + objectName + ", type=" + type + ", status=" + status + '}';
    }
  
  
    
}
