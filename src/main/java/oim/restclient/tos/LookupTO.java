/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oim.restclient.tos;

import java.util.List;

/**
 *
 * @author Owner
 */


public class LookupTO  {
    
    private String name;
    private List<OIMAttributeTO> fields;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<OIMAttributeTO> getFields() {
        return fields;
    }

    public void setFields(List<OIMAttributeTO> fields) {
        this.fields = fields;
    }

    @Override
    public String toString() {
        return "LookupTO{" + "name=" + name + ", fields=" + fields + '}';
    }
    
}
