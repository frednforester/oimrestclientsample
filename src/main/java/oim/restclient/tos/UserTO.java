/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oim.restclient.tos;

import java.util.List;

/**
 *
 * @author Owner
 */
public class UserTO {

    private String id;
    private String name;
    private String description;
    private String userLogin;
    private String displayName;
    private String email;
    private String startDate;
    private String endDate;
    private String requestId;
    private String status;
    private List<OIMAttributeTO> fields;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<OIMAttributeTO> getFields() {
        return fields;
    }

    public void setFields(List<OIMAttributeTO> fields) {
        this.fields = fields;
    }

    @Override
    public String toString() {
        return "UserTO{" + "id=" + id + ", name=" + name + ", userLogin=" + userLogin + ", email=" + email + ", startDate=" + startDate + ", endDate=" + endDate + ", requestId=" + requestId + ", status=" + status + '}';
    }
    
    

    
}
