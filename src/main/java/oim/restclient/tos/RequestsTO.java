/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oim.restclient.tos;

import java.util.List;

/**
 *
 * @author Owner
 */
public class RequestsTO {
    
    private List<RequestTO> requests;

    public List<RequestTO> getRequests() {
        return requests;
    }

    public void setRequests(List<RequestTO> requests) {
        this.requests = requests;
    }

    @Override
    public String toString() {
        return "RequestsTO{" + "requests=" + requests + '}';
    }
    
    
    
}
