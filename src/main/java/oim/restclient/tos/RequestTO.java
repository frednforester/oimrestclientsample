/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oim.restclient.tos;

import java.util.List;

/**
 *
 * @author Owner
 */
public class RequestTO {

    private String id;
    private String state;
    private String error;
    private String action;
    private String actionComment;
    private String requestId;
    private String reqStatus;
    private String status;
    private String reqAssignee;
    private OIMAttributeTO requester;
    private String reqBeneficiary;
    private String reqCreatedOn;
    private String reqExpireOn;
    private String reqJustification;
    private String reqApprovalType;
    private String reqType;
    private String userLogin;
    private String reason;
    private List<AssigneeTO> assignees;
    private String taskId;
    private String title;
    private String parentRequestId;
    
    private List<UserTO> reqBeneficiaryList;
    private List<TargetEntity> reqTargetEntities;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getActionComment() {
        return actionComment;
    }

    public void setActionComment(String actionComment) {
        this.actionComment = actionComment;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getReqStatus() {
        return reqStatus;
    }

    public void setReqStatus(String reqStatus) {
        this.reqStatus = reqStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReqAssignee() {
        return reqAssignee;
    }

    public void setReqAssignee(String reqAssignee) {
        this.reqAssignee = reqAssignee;
    }

    public OIMAttributeTO getRequester() {
        return requester;
    }

    public void setRequester(OIMAttributeTO requester) {
        this.requester = requester;
    }

    public String getReqBeneficiary() {
        return reqBeneficiary;
    }

    public void setReqBeneficiary(String reqBeneficiary) {
        this.reqBeneficiary = reqBeneficiary;
    }

    public String getReqCreatedOn() {
        return reqCreatedOn;
    }

    public void setReqCreatedOn(String reqCreatedOn) {
        this.reqCreatedOn = reqCreatedOn;
    }

    public String getReqExpireOn() {
        return reqExpireOn;
    }

    public void setReqExpireOn(String reqExpireOn) {
        this.reqExpireOn = reqExpireOn;
    }

    public String getReqJustification() {
        return reqJustification;
    }

    public void setReqJustification(String reqJustification) {
        this.reqJustification = reqJustification;
    }

    public String getReqApprovalType() {
        return reqApprovalType;
    }

    public void setReqApprovalType(String reqApprovalType) {
        this.reqApprovalType = reqApprovalType;
    }

    public String getReqType() {
        return reqType;
    }

    public void setReqType(String reqType) {
        this.reqType = reqType;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public List<AssigneeTO> getAssignees() {
        return assignees;
    }

    public void setAssignees(List<AssigneeTO> assignees) {
        this.assignees = assignees;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParentRequestId() {
        return parentRequestId;
    }

    public void setParentRequestId(String parentRequestId) {
        this.parentRequestId = parentRequestId;
    }

    public List<UserTO> getReqBeneficiaryList() {
        return reqBeneficiaryList;
    }

    public void setReqBeneficiaryList(List<UserTO> reqBeneficiaryList) {
        this.reqBeneficiaryList = reqBeneficiaryList;
    }

    public List<TargetEntity> getReqTargetEntities() {
        return reqTargetEntities;
    }

    public void setReqTargetEntities(List<TargetEntity> reqTargetEntities) {
        this.reqTargetEntities = reqTargetEntities;
    }

    @Override
    public String toString() {
        return "RequestTO{" + "id=" + id + ", state=" + state + ", error=" + error + ", action=" + action + ", requestId=" + requestId + ", reqStatus=" + reqStatus + ", status=" + status + ", requester=" + requester + ", reqBeneficiary=" + reqBeneficiary + ", reqApprovalType=" + reqApprovalType + ", reqType=" + reqType + ", userLogin=" + userLogin + ", assignees=" + assignees + ", parentRequestId=" + parentRequestId + ", reqBeneficiaryList=" + reqBeneficiaryList + ", reqTargetEntities=" + reqTargetEntities + '}';
    }

}
