/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oim.restclient.tos;

import java.util.List;

/**
 *
 * @author Owner
 */
public class OrganizationTO {
    
    private String id;
    private List<OIMAttributeTO> fields;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<OIMAttributeTO> getFields() {
        return fields;
    }

    public void setFields(List<OIMAttributeTO> fields) {
        this.fields = fields;
    }

    @Override
    public String toString() {
        return "OrganizationTO{" + "id=" + id + ", fields=" + fields + '}';
    }
    
    
    
    
}
