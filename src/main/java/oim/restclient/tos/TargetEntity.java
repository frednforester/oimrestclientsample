/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oim.restclient.tos;

import java.util.List;

/**
 *
 * @author Owner
 */
public class TargetEntity {

    private String entityName;
    private List<OIMAttributeTO> appFormData;
    private String entityId;
    private Long catalogId;
    private String entityType;
    private String parentAccountId;
    private String patentAccountName;
    private String startDate;
    private String endDate;

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public List<OIMAttributeTO> getAppFormData() {
        return appFormData;
    }

    public void setAppFormData(List<OIMAttributeTO> appFormData) {
        this.appFormData = appFormData;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public Long getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(Long catalogId) {
        this.catalogId = catalogId;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getParentAccountId() {
        return parentAccountId;
    }

    public void setParentAccountId(String parentAccountId) {
        this.parentAccountId = parentAccountId;
    }

    public String getPatentAccountName() {
        return patentAccountName;
    }

    public void setPatentAccountName(String patentAccountName) {
        this.patentAccountName = patentAccountName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "TargetEntity{" + "entityName=" + entityName + ", appFormData=" + appFormData + ", entityId=" + entityId + ", catalogId=" + catalogId + ", entityType=" + entityType + ", parentAccountId=" + parentAccountId + ", patentAccountName=" + patentAccountName + ", startDate=" + startDate + ", endDate=" + endDate + '}';
    }
    
    
    
}
