/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oim.restclient.tos;

/**
 *
 * @author Owner
 */
public class EntitlementTO {

    private String id;
    private String name;
    private String description;
    private String itResourseName;
    private OIMAttributeTO applicationInstanceName;
    private String formName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getItResourseName() {
        return itResourseName;
    }

    public void setItResourseName(String itResourseName) {
        this.itResourseName = itResourseName;
    }

    public OIMAttributeTO getApplicationInstanceName() {
        return applicationInstanceName;
    }

    public void setApplicationInstanceName(OIMAttributeTO applicationInstanceName) {
        this.applicationInstanceName = applicationInstanceName;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    @Override
    public String toString() {
        return "EntitlementTO{" + "id=" + id + ", name=" + name + ", description=" + description + ", itResourseName=" + itResourseName + ", applicationInstanceName=" + applicationInstanceName + ", formName=" + formName + '}';
    }
    
}
