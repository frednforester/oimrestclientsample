/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oim.restclient.exceptions;

/**
 *
 * @author Owner
 */
public class EntityNotFoundException extends Exception {

    public EntityNotFoundException() {
    }

    public EntityNotFoundException(String string) {
        super(string);
    }

    public EntityNotFoundException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public EntityNotFoundException(Throwable thrwbl) {
        super(thrwbl);
    }

    public EntityNotFoundException(String string, Throwable thrwbl, boolean bln, boolean bln1) {
        super(string, thrwbl, bln, bln1);
    }
    
    
}
