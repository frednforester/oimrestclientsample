/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oim.restclient.exceptions;

/**
 *
 * @author Owner
 */
public class TooManyEntitiesException extends Exception {

    public TooManyEntitiesException() {
    }

    public TooManyEntitiesException(String string) {
        super(string);
    }

    public TooManyEntitiesException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public TooManyEntitiesException(Throwable thrwbl) {
        super(thrwbl);
    }

    public TooManyEntitiesException(String string, Throwable thrwbl, boolean bln, boolean bln1) {
        super(string, thrwbl, bln, bln1);
    }
    
    
}
