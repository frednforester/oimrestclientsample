/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oim.restclient;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import oim.restclient.exceptions.TooManyEntitiesException;
import oim.restclient.exceptions.EntityNotFoundException;
import oim.restclient.tos.ApplicationTO;
import oim.restclient.tos.OIMAttributeTO;
import oim.restclient.tos.EntitlementTO;
import oim.restclient.tos.LookupTO;
import oim.restclient.tos.OrganizationTO;
import oim.restclient.tos.RequestOperationStatus;
import oim.restclient.tos.RequestTO;
import oim.restclient.tos.RequestsTO;
import oim.restclient.tos.RoleTO;
import oim.restclient.tos.TargetEntity;
import oim.restclient.tos.UserTO;
import oim.restclient.tos.UsersTO;
//import oracle.iam.rest.tos.OIMAttributeTO;
//import oracle.iam.rest.tos.OIMAttributeTO;
//import oracle.iam.rest.tos.OrganizationTO;
//import oracle.iam.rest.tos.RequestTO;
//import oracle.iam.rest.tos.RequestsTO;
//import oracle.iam.rest.tos.RoleTO;
//import oracle.iam.rest.tos.TargetEntity;
//import oracle.iam.rest.tos.UserTO;
//import oracle.iam.rest.tos.UsersTO;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

/**
 *
 * @author Owner
 */
public class OIMRestAPI {
    
    //http://oim.testserver.org:14000
    private String userEndPoint = "<HOSTNAME>/iam/governance/selfservice/api/v1/users";
    private String reqEndPoint = "<HOSTNAME>/iam/governance/selfservice/api/v1/requests";
    private String aiEndPoint =  "<HOSTNAME>/iam/governance/selfservice/api/v1/applications";  
    private String roleEndPoint = "<HOSTNAME>/iam/governance/selfservice/api/v1/roles";
    private String orgEndPoint = "<HOSTNAME>/iam/governance/selfservice/api/v1/organizations";
    private String entEndPoint = "<HOSTNAME>/iam/governance/selfservice/api/v1/entitlements";
    private String customLkupEndPoint = "<HOSTNAME>/iam/governance/selfservice/api/v1/customlookups";
    private String customRolesEndPoint = "<HOSTNAME>/iam/governance/selfservice/api/v1/customroles";
    private String customRequestEndPoint = "<HOSTNAME>/iam/governance/selfservice/api/v1/customrequests";
    
    public static final String ORG_STATUS = "OrgStatus";
    
    private String authString;
    private String hostName;
    
    private Logger logger = Logger.getLogger(this.getClass().getName());

    /**
     * Construct the RestAPI Wrapper
     * @param userId - oim admin user. example. xelsysadm
     * @param password - oim admin user password
     * @param url - the base url to OIM. eg. http://oim.testserver.org:14000
     * @throws Exception 
     */
    public OIMRestAPI(String userId,String password,String url) throws Exception 
    {
        if (userId == null || password == null || url == null)
        {
            throw new Exception("Invalid parms");
        }
        Base64 encoder = new Base64();
        String auth = userId + ":" + password;
        authString = encoder.encodeToString(auth.getBytes());
        this.hostName = url;
        
    }
    
    /**
     * Returns a Map containing all the OIM users attributes
     * @param isLogin - is the parm the ID or the Login
     * @param searchKey - usr_key or usr_login
     * @return Map of all user attributes.
     * @throws Exception
     * @throws EntityNotFoundException
     * @throws TooManyEntitiesException 
     */
    public Map getUserByIdOrLogin(boolean isLogin,String searchKey) throws Exception,EntityNotFoundException,TooManyEntitiesException
    {
        
        if (searchKey == null)
            return null;
        
        String searchUrl = "";
        if (isLogin)
        {
            searchUrl = this.userEndPoint + "?q=User::Login eq " + searchKey;
        }
        else
        {
            searchUrl = this.userEndPoint + "/" + searchKey;
        }
        HttpURLConnection httpConn = this.getGetConnection(searchUrl);
        int respCode = httpConn.getResponseCode();
        String msg = httpConn.getResponseMessage();
        logger.debug("respcode:" + respCode);
        logger.debug("message:" + msg);
        
        try
        {
            ErrorHandler.checkResponse(respCode, msg);
        }
        catch(Exception e)
        {
            throw e;
        }
        
        
        ObjectMapper mapper = this.getObjectMapper();
        JsonNode rootNode = mapper.readTree(httpConn.getInputStream());
        JsonNode usersNode = rootNode.get("users");
        UserTO[] userTos = null;
        
        String jsonAllData = mapper.writeValueAsString(rootNode);
        logger.debug("JSON rootNode:" + jsonAllData);
        
        if (usersNode == null)
        {
            UserTO user = mapper.treeToValue(rootNode, UserTO.class);
            userTos = new UserTO[1];
            userTos[0] = user;
        }
        else
        {
            userTos = mapper.treeToValue(usersNode, UserTO[].class);
        }
        
        if (userTos.length == 0)
        {
            logger.error("ERROR User not found:" + searchKey);
            throw new EntityNotFoundException("ERROR User not found:" + searchKey);
        }
        if (userTos.length != 1)
        {
            logger.error("ERROR Too Many User Matches:" + searchKey);
            throw new TooManyEntitiesException("ERROR Too Many User Matches:" + searchKey);
        }
        String jsonData = mapper.writeValueAsString(userTos);
        logger.debug("JSON Users:" + jsonData);
        List<Map> mappedUsers = new ArrayList();
        for(UserTO user : userTos)
        {
            logger.debug("ID:" + user.getId());
            logger.debug("Fields:" + user.getFields());
            Map userMap = new HashMap();
            List<OIMAttributeTO> fields = user.getFields();
            for(OIMAttributeTO field : fields)
            {
                
                logger.debug("Name:" + field.getName());
                logger.debug("Value:" + field.getValue());
                userMap.put(field.getName(),field.getValue());
                
            }
            userMap.put("ID", user.getId());
            mappedUsers.add(userMap);
        }
        logger.debug("Users:" + mappedUsers);
        String output = null;
        httpConn.disconnect();
        if (!mappedUsers.isEmpty())
            return mappedUsers.get(0);
        else
            return null;
    }
    
    /**
     * get the OIM users usr_key
     * @param userLogin - the OIM usr_login
     * @return OIM usr_key of the user
     * @throws Exception
     * @throws EntityNotFoundException
     * @throws TooManyEntitiesException 
     */
    public String getUserKeyByLogin(String userLogin) throws Exception,EntityNotFoundException,TooManyEntitiesException
    {
        String searchUrl = this.userEndPoint + "?q=User::Login eq " + userLogin;
        HttpURLConnection httpConn = this.getGetConnection(searchUrl);
        int respCode = httpConn.getResponseCode();
        String msg = httpConn.getResponseMessage();
        logger.debug("respcode:" + respCode);
        logger.debug("message:" + msg);
        
        try
        {
            ErrorHandler.checkResponse(respCode, msg);
        }
        catch(Exception e)
        {
            throw e;
        }
        
        ObjectMapper mapper = this.getObjectMapper();
        JsonNode rootNode = mapper.readTree(httpConn.getInputStream());
        JsonNode usersNode = rootNode.get("users");
        UserTO[] userTos = null;
        
        String jsonAllData = mapper.writeValueAsString(rootNode);
        logger.debug("JSON rootNode:" + jsonAllData);
        
        if (usersNode == null)
        {
            UserTO user = mapper.treeToValue(rootNode, UserTO.class);
            userTos = new UserTO[1];
            userTos[0] = user;
        }
        else
        {
            userTos = mapper.treeToValue(usersNode, UserTO[].class);
        }
        
        if (userTos.length == 0)
        {
            logger.error("ERROR User not found:" + userLogin);
            throw new EntityNotFoundException("ERROR User not found:" + userLogin);
        }
        
        if (userTos.length != 1)
        {
            logger.error("ERROR Too Many User Matches");
            throw new TooManyEntitiesException("ERROR Too Many User Matches");
        }
        
        String jsonData = mapper.writeValueAsString(userTos);
        logger.debug("JSON Users:" + jsonData);
        List<Map> mappedUsers = new ArrayList();
        for(UserTO user : userTos)
        {
            logger.debug("ID:" + user.getId());
            logger.debug("Fields:" + user.getFields());
            Map userMap = new HashMap();
            List<OIMAttributeTO> fields = user.getFields();
            for(OIMAttributeTO field : fields)
            {
                
                logger.debug("Name:" + field.getName());
                logger.debug("Value:" + field.getValue());
                userMap.put(field.getName(),field.getValue());
                
            }
            userMap.put("ID", user.getId());
            mappedUsers.add(userMap);
        }
        logger.debug("Users:" + mappedUsers);
        String output = null;
        httpConn.disconnect();
        
        if (mappedUsers.isEmpty())
        {
            return null;
        }
        
        Map userMap = mappedUsers.get(0);
        return (String)userMap.get("ID");
        
    }
    
    /**
     * get usr_login by searching for the users common name
     * @param userCn - usr_common_name
     * @return the OIM usr_login
     * @throws Exception
     * @throws EntityNotFoundException
     * @throws TooManyEntitiesException 
     */
    public String findUserLoginByCN(String userCn) throws Exception,EntityNotFoundException,TooManyEntitiesException
    {
        String searchUrl = "";
        searchUrl = this.userEndPoint + "?q=UserCertCN eq " + userCn;
        HttpURLConnection httpConn = this.getGetConnection(searchUrl);
        int respCode = httpConn.getResponseCode();
        String msg = httpConn.getResponseMessage();
        logger.debug("respcode:" + respCode);
        logger.debug("message:" + msg);
        
        try
        {
            ErrorHandler.checkResponse(respCode, msg);
        }
        catch(Exception e)
        {
            throw e;
        }
        
        ObjectMapper mapper = this.getObjectMapper();
        JsonNode rootNode = mapper.readTree(httpConn.getInputStream());
        JsonNode usersNode = rootNode.get("users");
        UserTO[] userTos = null;
        
        String jsonAllData = mapper.writeValueAsString(rootNode);
        logger.debug("JSON rootNode:" + jsonAllData);
        
        if (usersNode == null)
        {
            UserTO user = mapper.treeToValue(rootNode, UserTO.class);
            userTos = new UserTO[1];
            userTos[0] = user;
        }
        else
        {
            userTos = mapper.treeToValue(usersNode, UserTO[].class);
        }
        
        if (userTos.length == 0)
        {
            logger.error("ERROR User not found:" + userCn);
            throw new EntityNotFoundException("ERROR User not found:" + userCn);
        }
        
        if (userTos.length != 1)
        {
            logger.error("ERROR Too Many User Matches:" + userCn);
            throw new TooManyEntitiesException("ERROR Too Many User Matches:" + userCn);
        }
        String jsonData = mapper.writeValueAsString(userTos);
        logger.debug("JSON Users:" + jsonData);
        List<Map> mappedUsers = new ArrayList();
        for(UserTO user : userTos)
        {
            logger.debug("ID:" + user.getId());
            logger.debug("Fields:" + user.getFields());
            Map userMap = new HashMap();
            List<OIMAttributeTO> fields = user.getFields();
            for(OIMAttributeTO field : fields)
            {
                
                logger.debug("Name:" + field.getName());
                logger.debug("Value:" + field.getValue());
                userMap.put(field.getName(),field.getValue());
                
            }
            userMap.put("ID", user.getId());
            mappedUsers.add(userMap);
        }
        logger.debug("Users:" + mappedUsers);
        httpConn.disconnect();
        if (mappedUsers.isEmpty())
            return null;
        
        Map user = mappedUsers.get(0);
        return (String)user.get("User Login");
    }
    
    /**
     * get the OIM users Organization status
     * @param userID - the OIM usr_login
     * @return usr_udf_org_status
     * @throws Exception 
     */
    public String getUserOrgStatus(String userID) throws Exception
    {
        Map userMap = this.getUserByIdOrLogin(true, userID);
        if (userMap == null)
            return null;
        String orgStatus = (String)userMap.get(OIMRestAPI.ORG_STATUS);
        return orgStatus;
    }
    
    /**
     * get the OIM users Organization Name
     * @param userID - usr_login of the OIM user
     * @return OIM User Organization Name
     * @throws Exception 
     */
    public String getUserOrganization(String userID) throws Exception 
    {
        Map userMap = this.getUserByIdOrLogin(true, userID);
        if (userMap == null)
            return null;
        String orgName = (String)userMap.get("Organization Name");
        return orgName;
    }
    
    /**
     * get a specific attribute from the OIM user record
     * @param userID - OIM usr_login
     * @param attributeName - OIM attribute in the USR record
     * @return the attribute value
     * @throws Exception 
     */
    public String getUserAttribute(String userID,String attributeName) throws Exception
    {
        Map userMap = this.getUserByIdOrLogin(true, userID);
        if (userMap == null)
            return null;
        String attributeValue = (String)userMap.get(attributeName);
        return attributeValue;
    }
    
    /**
     * update the OIM users Organization Status
     * @param userId - OIM usr_login
     * @param orgStatus - the new Organization status
     * @throws Exception 
     */
    public void setUserOrgStatus(String userId,String orgStatus) throws Exception
    {
        if (userId == null || orgStatus == null)
        {
            throw new Exception("Invalid Parms");
        }
        
        Map userMap = this.getUserByIdOrLogin(true, userId);
        if (userMap == null || userMap.isEmpty())
        {
            throw new Exception("Invalid User");
        }
        
        String usrKey = (String)userMap.get("ID");
        Map updMap = new HashMap();
        updMap.put(OIMRestAPI.ORG_STATUS,orgStatus);
        this.updateUserByIdUsrKey(usrKey, updMap);
    }
    
    /**
     * update the OIM users organization
     * @param userId - the OIM usr_login
     * @param orgName - OIM Organization Name to be assigned to the USR
     * @throws Exception
     * @throws EntityNotFoundException 
     */
    public void assignOrganizationToUser(String userId,String orgName) throws Exception,EntityNotFoundException
    {
        if (userId == null || orgName == null)
        {
            throw new Exception("Invalid Parms");
        }
        Map updMap = new HashMap();
        Map orgMap = this.getOrganizationByName(orgName);
        if (orgMap == null || orgMap.isEmpty())
        {
            throw new EntityNotFoundException("Invalid Organization:" + orgName);
        }
        Map userMap = this.getUserByIdOrLogin(true, userId);
        if (userMap == null || userMap.isEmpty())
        {
            throw new EntityNotFoundException("Invalid User:" + userId);
        }
        
        String orgId = (String)orgMap.get("ID");
        String usrKey = (String)userMap.get("ID");
        updMap.put("act_key",orgId);
        //updMap.put("Organization Name",orgName);
        this.updateUserByIdUsrKey(usrKey, updMap);
        
    }
    
    /**
     * Update the OIM user attributes specified in the data Map.
     * @param usrKey - the OIM usr_key
     * @param data - Key Value pairs of the new USR attributes.
     * @throws Exception
     * @throws EntityNotFoundException
     * @throws TooManyEntitiesException 
     */
    public void updateUserByIdUsrKey(String usrKey,Map data) throws Exception,EntityNotFoundException,TooManyEntitiesException
    {
        //String updUrl = this.userEndPoint + "/" + usrKey;
        String updUrl = this.userEndPoint;
        HttpURLConnection httpConn = this.getPutConnection(updUrl);
        List<UserTO> userList = new ArrayList();
        UsersTO userTos = new UsersTO();
        UserTO user = new UserTO();
        
        List<OIMAttributeTO> fields = new ArrayList();
        Set<String> keys = data.keySet();
        for(String key : keys)
        {
            OIMAttributeTO attr = new OIMAttributeTO();
            attr.setName(key);
            attr.setValue(data.get(key));
            fields.add(attr);
        }
        user.setRequestId(usrKey);
        user.setId(usrKey);
        user.setFields(fields);
        userList.add(user);
        userTos.setUsers(userList);
        ObjectMapper mapper = this.getObjectMapper();
        String jsonString = mapper.writeValueAsString(userTos);
        logger.debug("jsonString:" + jsonString);
        OutputStreamWriter out = new OutputStreamWriter(httpConn.getOutputStream());
        out.write(jsonString);
        out.close();
        
        int respCode = httpConn.getResponseCode();
        String msg = httpConn.getResponseMessage();
        logger.debug("respcode:" + respCode);
        logger.debug("message:" + msg);
        httpConn.disconnect();
        
        try
        {
            ErrorHandler.checkResponse(respCode, msg);
        }
        catch(Exception e)
        {
            throw e;
        }
    }
    
    /**
     * get an OIM Organization by Name
     * @param orgName - the OIM organization name
     * @return Map of the Organization attributes
     * @throws Exception
     * @throws EntityNotFoundException
     * @throws TooManyEntitiesException 
     */
    public Map getOrganizationByName(String orgName) throws Exception,EntityNotFoundException,TooManyEntitiesException
    {
        String searchUrl = this.orgEndPoint + "?q=Organization::Name eq " + orgName;
        HttpURLConnection httpConn = this.getGetConnection(searchUrl);
        
        int respCode = httpConn.getResponseCode();
        String msg = httpConn.getResponseMessage();
        logger.debug("respcode:" + respCode);
        logger.debug("message:" + msg);
        try
        {
            ErrorHandler.checkResponse(respCode, msg);
        }
        catch(Exception e)
        {
            throw e;
        }
        
        ObjectMapper mapper = this.getObjectMapper();
        JsonNode rootNode = mapper.readTree(httpConn.getInputStream());
        JsonNode orgNode = rootNode.get("organizations");
        OrganizationTO[] orgTos = null;
        if (orgNode == null)
        {
            OrganizationTO org = mapper.treeToValue(rootNode, OrganizationTO.class);
            orgTos = new OrganizationTO[1];
            orgTos[0] = org;
        }
        else
        {
            orgTos = mapper.treeToValue(orgNode, OrganizationTO[].class);
        }
        
        if (orgTos.length == 0)
        {
            logger.error("ERROR Org not found:" + orgName);
            throw new EntityNotFoundException("ERROR Org not found:" + orgName);
        }
        
        if (orgTos.length != 1)
        {
            logger.error("ERROR Too Many Org Matches:" + orgName);
            throw new TooManyEntitiesException("ERROR Too Many User Matches:" + orgName);
        }
        List<Map> mappedRoles = new ArrayList();
        
        for(OrganizationTO org : orgTos)
        {
            Map orgMap = new HashMap();
            logger.debug("Org:" + org.getFields());
            List<OIMAttributeTO> fields = org.getFields();
            for(OIMAttributeTO field : fields)
            {
                
                logger.debug("Name:" + field.getName());
                logger.debug("Value:" + field.getValue());
                orgMap.put(field.getName(),field.getValue());
                
            }
            orgMap.put("ID", org.getId());
            mappedRoles.add(orgMap);
        }
        
        logger.debug("Orgs:" + mappedRoles);
        httpConn.disconnect();
        if (!mappedRoles.isEmpty())
            return mappedRoles.get(0);
        else
            return null;
        
    }
    
    /**
     * get the OIM role based on the role name. this uses an Search Query in which
     * case the roleName must replace spaces with colons internally.
     * @param roleName - the name of the OIM role.
     * @return Map of the OIM Role Attributes.
     * @throws Exception
     * @throws EntityNotFoundException
     * @throws TooManyEntitiesException 
     */
    @Deprecated
    public Map getRoleByName(String roleName) throws Exception,EntityNotFoundException,TooManyEntitiesException
    {
        if (roleName == null)
            return null;
        roleName = roleName.replace(" ", "::");
        String searchUrl = this.roleEndPoint + "?q=Role::Name eq " + roleName;
        HttpURLConnection httpConn = this.getGetConnection(searchUrl);
        
        int respCode = httpConn.getResponseCode();
        String msg = httpConn.getResponseMessage();
        logger.debug("respcode:" + respCode);
        logger.debug("message:" + msg);
        try
        {
            ErrorHandler.checkResponse(respCode, msg);
        }
        catch(Exception e)
        {
            throw e;
        }
        ObjectMapper mapper = this.getObjectMapper();
        JsonNode rootNode = mapper.readTree(httpConn.getInputStream());
        JsonNode roleNode = rootNode.get("roles");
        
        logger.debug("root:" + rootNode);
        logger.debug("node:" + roleNode);
        RoleTO[] roleTos = null;
        if (roleNode == null)
        {
            RoleTO role = mapper.treeToValue(rootNode, RoleTO.class);
            roleTos = new RoleTO[1];
            roleTos[0] = role;
        }
        else
        {
            roleTos = mapper.treeToValue(roleNode, RoleTO[].class);
        }
        
        if (roleTos.length == 0)
        {
            logger.error("ERROR Role Not Found:" + roleName);
            throw new EntityNotFoundException("ERROR Role Not Found");
        }
        
        if (roleTos.length != 1)
        {
            logger.error("ERROR Too Many Role Matches:" + roleName);
            throw new TooManyEntitiesException("ERROR Too Many Role Matches");
        }
        
        List<Map> retRoles = new ArrayList();
        for(RoleTO role : roleTos)
        {
            Map roleMap = new HashMap();
            logger.debug("Role:" + role.getFields());
            List<OIMAttributeTO> fields = role.getFields();
            for(OIMAttributeTO field : fields)
            {
                
                logger.debug("Name:" + field.getName());
                logger.debug("Value:" + field.getValue());
                roleMap.put(field.getName(),field.getValue());
                
            }
            roleMap.put("ID", role.getId());
            retRoles.add(roleMap);
        }
        
        httpConn.disconnect();
        if (!retRoles.isEmpty())
            return retRoles.get(0);
        else
            return null;
    }
    
    /**
     * get the OIM role banes on the RoleName.
     * @param roleName - name of the OIM role to find
     * @return
     * @throws Exception
     * @throws EntityNotFoundException
     * @throws TooManyEntitiesException 
     */
    public Map getRoleByRoleName(String roleName) throws Exception,EntityNotFoundException,TooManyEntitiesException
    {
        
        String searchUrl = this.customRolesEndPoint + "/name/" + roleName;
        HttpURLConnection httpConn = this.getGetConnection(searchUrl);
        
        int respCode = httpConn.getResponseCode();
        String msg = httpConn.getResponseMessage();
        logger.debug("respcode:" + respCode);
        logger.debug("message:" + msg);
        try
        {
            ErrorHandler.checkResponse(respCode, msg);
        }
        catch(Exception e)
        {
            throw e;
        }
        ObjectMapper mapper = this.getObjectMapper();
        JsonNode rootNode = mapper.readTree(httpConn.getInputStream());
        JsonNode roleNode = rootNode.get("roles");
        
        logger.debug("root:" + rootNode);
        logger.debug("node:" + roleNode);
        RoleTO[] roleTos = null;
        if (roleNode == null)
        {
            RoleTO role = mapper.treeToValue(rootNode, RoleTO.class);
            roleTos = new RoleTO[1];
            roleTos[0] = role;
        }
        else
        {
            roleTos = mapper.treeToValue(roleNode, RoleTO[].class);
        }
        
        if (roleTos.length == 0)
        {
            logger.error("ERROR Role Not Found:" + roleName);
            throw new EntityNotFoundException("ERROR Role Not Found");
        }
        
        if (roleTos.length != 1)
        {
            logger.error("ERROR Too Many Role Matches:" + roleName);
            throw new TooManyEntitiesException("ERROR Too Many Role Matches");
        }
        
        List<Map> retRoles = new ArrayList();
        for(RoleTO role : roleTos)
        {
            Map roleMap = new HashMap();
            logger.debug("Role:" + role.getFields());
            List<OIMAttributeTO> fields = role.getFields();
            for(OIMAttributeTO field : fields)
            {
                
                logger.debug("Name:" + field.getName());
                logger.debug("Value:" + field.getValue());
                roleMap.put(field.getName(),field.getValue());
                
            }
            roleMap.put("ID", role.getId());
            retRoles.add(roleMap);
        }
        
        httpConn.disconnect();
        if (!retRoles.isEmpty())
            return retRoles.get(0);
        else
            return null;
    }
    
    /**
     * get all the roles that a user is a member of.
     * @param userLogin - the OIM usr_login
     * @return List of Role Names
     * @throws Exception 
     */
    public List<String> getUserRoles(String userLogin) throws Exception
    {
        if (userLogin == null)
            return null;
        
        String usrKey = this.getUserKeyByLogin(userLogin);
        
        String searchUrl = this.customRolesEndPoint + "/memberships/" + usrKey;
        HttpURLConnection httpConn = this.getGetConnection(searchUrl);
        
        int respCode = httpConn.getResponseCode();
        String msg = httpConn.getResponseMessage();
        logger.debug("respcode:" + respCode);
        logger.debug("message:" + msg);
        try
        {
            ErrorHandler.checkResponse(respCode, msg);
        }
        catch(Exception e)
        {
            throw e;
        }
        ObjectMapper mapper = this.getObjectMapper();
        JsonNode rootNode = mapper.readTree(httpConn.getInputStream());
        JsonNode roleNode = rootNode.get("roles");
        RoleTO[] roleTos = null;
        if (roleNode == null)
        {
            RoleTO role = mapper.treeToValue(rootNode, RoleTO.class);
            roleTos = new RoleTO[1];
            roleTos[0] = role;
        }
        else
        {
            roleTos = mapper.treeToValue(roleNode, RoleTO[].class);
        }
        
        //List<Map> retRoles = new ArrayList();
        List<String> roleNames = new ArrayList();
        for(RoleTO role : roleTos)
        {
            //Map roleMap = new HashMap();
            logger.debug("Role:" + role.getFields());
            List<OIMAttributeTO> fields = role.getFields();
            for(OIMAttributeTO field : fields)
            {
                
                logger.debug("Name:" + field.getName());
                logger.debug("Value:" + field.getValue());
                //roleMap.put(field.getName(),field.getValue());
                if (field.getName().equalsIgnoreCase("Role Name"))
                {
                    String name = (String)field.getValue();
                    roleNames.add(name);
                }
                
            }
            //roleMap.put("ID", role.getid());
            //retRoles.add(roleMap);
        }
        
        httpConn.disconnect();
        return roleNames;
    }
    
    /**
     * get the users roles based on the roles catalog category.
     * @param category - the Category Name
     * @param userID - the OIM usr_login
     * @return
     * @throws Exception 
     */
    public List getUserRolesByCatalogCategory(String category, String userID) throws Exception
    {
        if (category == null || userID ==  null)
            return null;
        String searchUrl = this.customRolesEndPoint + "/categories/userlogin/" + userID + "/" + category;
        HttpURLConnection httpConn = this.getGetConnection(searchUrl);
        
        int respCode = httpConn.getResponseCode();
        String msg = httpConn.getResponseMessage();
        logger.debug("respcode:" + respCode);
        logger.debug("message:" + msg);
        try
        {
            ErrorHandler.checkResponse(respCode, msg);
        }
        catch(Exception e)
        {
            throw e;
        }
        ObjectMapper mapper = this.getObjectMapper();
        JsonNode rootNode = mapper.readTree(httpConn.getInputStream());
        JsonNode roleNode = rootNode.get("roles");
        RoleTO[] roleTos = null;
        if (roleNode == null)
        {
            RoleTO role = mapper.treeToValue(rootNode, RoleTO.class);
            roleTos = new RoleTO[1];
            roleTos[0] = role;
        }
        else
        {
            roleTos = mapper.treeToValue(roleNode, RoleTO[].class);
        }
        
        //List<Map> retRoles = new ArrayList();
        List<String> roleNames = new ArrayList();
        for(RoleTO role : roleTos)
        {
            //Map roleMap = new HashMap();
            logger.debug("Role:" + role.getFields());
            List<OIMAttributeTO> fields = role.getFields();
            for(OIMAttributeTO field : fields)
            {
                
                logger.debug("Name:" + field.getName());
                logger.debug("Value:" + field.getValue());
                //roleMap.put(field.getName(),field.getValue());
                if (field.getName().equalsIgnoreCase("Role Name"))
                {
                    String name = (String)field.getValue();
                    roleNames.add(name);
                }
                
            }
            //roleMap.put("ID", role.getid());
            //retRoles.add(roleMap);
        }
        
        httpConn.disconnect();
        return roleNames;
        
    }
    
    /**
     * get application by name.
     * @param appName - the name of the OIM Application Instance
     * @return - ApplicationTO object
     * @throws Exception
     * @throws EntityNotFoundException
     * @throws TooManyEntitiesException 
     */
    public ApplicationTO getApplicationByName(String appName) throws Exception,EntityNotFoundException,TooManyEntitiesException
    {
        if (appName == null)
            return null;
        appName = appName.replace(" ","::");
        String searchUrl = this.aiEndPoint + "?q=appInstanceName eq " + appName;
        HttpURLConnection httpConn = this.getGetConnection(searchUrl);
        
        int respCode = httpConn.getResponseCode();
        String msg = httpConn.getResponseMessage();
        logger.debug("respcode:" + respCode);
        logger.debug("message:" + msg);
        try
        {
            ErrorHandler.checkResponse(respCode, msg);
        }
        catch(Exception e)
        {
            throw e;
        }
        
        ObjectMapper mapper = this.getObjectMapper();
        ApplicationTO[] appTos = null;
        JsonNode rootNode = mapper.readTree(httpConn.getInputStream());
        logger.debug("rootnode:" + mapper.writeValueAsString(rootNode));
        JsonNode appNode = rootNode.get("applications");
        if (appNode == null)
        {
            ApplicationTO app = mapper.treeToValue(rootNode, ApplicationTO.class);
            appTos = new ApplicationTO[1];
            appTos[0] = app;
        }
        else
        {
            appTos = mapper.treeToValue(appNode, ApplicationTO[].class);
        }
        
        if (appTos.length == 0)
        {
            logger.error("ERROR Application Not Found:" + appName);
            throw new EntityNotFoundException("ERROR Application Not Found:" + appName);
        }
        
        if (appTos.length != 1)
        {
            logger.error("ERROR Too Many App Matches:" + appName);
            throw new TooManyEntitiesException("ERROR Too Many User Matches:" + appName);
        }
        
        ApplicationTO app = appTos[0];
        httpConn.disconnect();
        return this.getApplicationById(app.getId());
    }
    
    /**
     * get OIM Application Instance by ID.
     * @param appId - the ID of the OIM Application Instance
     * @return - ApplicationTO object
     * @throws Exception
     * @throws TooManyEntitiesException
     * @throws EntityNotFoundException 
     */
    public ApplicationTO getApplicationById(String appId) throws Exception,TooManyEntitiesException,EntityNotFoundException
    {
        
        if (appId == null)
        {
            return null;
        }
        String searchUrl = this.aiEndPoint + "/" + appId;
        HttpURLConnection httpConn = this.getGetConnection(searchUrl);
        
        int respCode = httpConn.getResponseCode();
        String msg = httpConn.getResponseMessage();
        logger.debug("respcode:" + respCode);
        logger.debug("message:" + msg);
        try
        {
            ErrorHandler.checkResponse(respCode, msg);
        }
        catch(Exception e)
        {
            throw e;
        }
       
        ObjectMapper mapper = this.getObjectMapper();
        ApplicationTO[] appTos = null;
        JsonNode rootNode = mapper.readTree(httpConn.getInputStream());
        logger.debug("rootnode:" + mapper.writeValueAsString(rootNode));
        JsonNode appNode = rootNode.get("applications");
        if (appNode == null)
        {
            ApplicationTO app = mapper.treeToValue(rootNode, ApplicationTO.class);
            appTos = new ApplicationTO[1];
            appTos[0] = app;
        }
        else
        {
            appTos = mapper.treeToValue(appNode, ApplicationTO[].class);
        }
        
        if (appTos.length == 0)
        {
            logger.error("ERROR Application Not Found:" + appId);
            throw new EntityNotFoundException("ERROR Application Not Found:" + appId);
        }
        
        if (appTos.length != 1)
        {
            logger.error("ERROR Too Many App Matches:" + appId);
            throw new TooManyEntitiesException("ERROR Too Many User Matches:" + appId);
        }
        
        ApplicationTO app = appTos[0];
        httpConn.disconnect();
        return app;
    }
    
    /**
     * get the OIM Entitlement based on name and OIM Resource Object Key.
     * @param entName - OIM Entitlement Name
     * @param objKey - obj_key of the OIM Resource Object
     * @return
     * @throws Exception
     * @throws TooManyEntitiesException
     * @throws EntityNotFoundException 
     */
    public EntitlementTO getEntitlementByNameObj(String entName,String objKey) throws Exception,TooManyEntitiesException,EntityNotFoundException
    {
        if (entName == null || objKey == null)
            return null;
        entName = entName.replace(" ", "::");
        objKey = objKey.replace(" ", "::");
        String searchUrl = this.entEndPoint + "?q=entitlementCode eq " + entName + " and objectKey eq " + objKey;
        HttpURLConnection httpConn = this.getGetConnection(searchUrl);
        
        int respCode = httpConn.getResponseCode();
        String msg = httpConn.getResponseMessage();
        logger.debug("respcode:" + respCode);
        logger.debug("message:" + msg);
        try
        {
            ErrorHandler.checkResponse(respCode, msg);
        }
        catch(Exception e)
        {
            throw e;
        }
       
        ObjectMapper mapper = this.getObjectMapper();
        EntitlementTO[] entTos = null;
        JsonNode rootNode = mapper.readTree(httpConn.getInputStream());
        logger.debug("rootnode:" + mapper.writeValueAsString(rootNode));
        JsonNode appNode = rootNode.get("entitlements");
        if (appNode == null)
        {
            EntitlementTO app = mapper.treeToValue(rootNode, EntitlementTO.class);
            entTos = new EntitlementTO[1];
            entTos[0] = app;
        }
        else
        {
            entTos = mapper.treeToValue(appNode, EntitlementTO[].class);
        }
        if (entTos.length != 1)
        {
            logger.error("ERROR Entitlement Not Found:" + entName + ":" + objKey);
            throw new EntityNotFoundException("ERROR Entitlement Not Found:" + entName + ":" + objKey);
        }
        
        if (entTos.length != 1)
        {
            logger.error("ERROR Too Many Entitlement Matches:" + entName + ":" + objKey);
            throw new TooManyEntitiesException("ERROR Too Many Entitlement Matches:" + entName + ":" + objKey);
        }
        
        EntitlementTO ent = entTos[0];
        return ent;
    }
    
    /**
     * get the values of the OIM Lookup
     * @param lookupName - Name of the OIM Lookup table to return
     * @return Map of the Code DeCode values
     * @throws Exception
     * @throws TooManyEntitiesException
     * @throws EntityNotFoundException 
     */
    public LookupTO getLookupTable(String lookupName) throws Exception,TooManyEntitiesException,EntityNotFoundException
    {
        if (lookupName == null)
            return null;
        lookupName = lookupName.replace(" ","::");
        String searchUrl = this.customLkupEndPoint + "?q=lookupName eq " + lookupName;
        HttpURLConnection httpConn = this.getGetConnection(searchUrl);
        
        int respCode = httpConn.getResponseCode();
        String msg = httpConn.getResponseMessage();
        logger.debug("respcode:" + respCode);
        logger.debug("message:" + msg);
        try
        {
            ErrorHandler.checkResponse(respCode, msg);
        }
        catch(Exception e)
        {
            throw e;
        }
        
        ObjectMapper mapper = this.getObjectMapper();
        LookupTO[] lkupTos = null;
        JsonNode rootNode = mapper.readTree(httpConn.getInputStream());
        logger.debug("rootnode:" + mapper.writeValueAsString(rootNode));
        JsonNode lkupNode = rootNode.get("lookups");
        if (lkupNode == null)
        {
            LookupTO app = mapper.treeToValue(rootNode, LookupTO.class);
            lkupTos = new LookupTO[1];
            lkupTos[0] = app;
        }
        else
        {
            lkupTos = mapper.treeToValue(lkupNode, LookupTO[].class);
        }
        
        if (lkupTos.length == 0)
        {
            logger.error("ERROR Lookup Not Found:" + lookupName);
            throw new EntityNotFoundException("ERROR Lookup Not Found:" + lookupName);
        }
        
        if (lkupTos.length != 1)
        {
            logger.error("ERROR Too Many Lookup Matches:" + lookupName);
            throw new TooManyEntitiesException("ERROR Too Many Lookup Matches:" + lookupName);
        }
        
        LookupTO ent = lkupTos[0];
        return ent;
        
    }
    
    /**
     * create an OIM Request
     * @param justif - Justification for the request
     * @param beneficiaryId - Beneficiary ID. usr_key of the OIM user
     * @param entityId - Entity Key
     * @param entityType - Entity Type
     * @return
     * @throws Exception 
     */
    public String createRequest(String justif,String beneficiaryId,String entityId,String entityType) throws Exception 
    {
        HttpURLConnection httpConn = this.getPostConnection(this.reqEndPoint);
        
        RequestsTO reqsTo = new RequestsTO();
        RequestTO reqTo = new RequestTO();
        UserTO beneficiary = new UserTO();
        List<UserTO> beneficiaries = new ArrayList();
        beneficiary.setId(beneficiaryId);
        beneficiaries.add(beneficiary);
        List<RequestTO> reqTos = new ArrayList();
        List<TargetEntity> entities = new ArrayList();
        TargetEntity ta1 = new TargetEntity();
        reqTo.setReqJustification(justif);
        //reqTo.setReqBeneficiary(beneficiaryId);
        //reqTo.setRequestId("99"+beneficiaryId);
        reqTo.setReqBeneficiaryList(beneficiaries);
        ta1.setEntityId(entityId);
        ta1.setEntityType(entityType);
        entities.add(ta1);
        reqTo.setReqTargetEntities(entities);
        
        reqTos.add(reqTo);
        reqsTo.setRequests(reqTos);
        
        ObjectMapper mapper = this.getObjectMapper();
        String jsonString = mapper.writeValueAsString(reqsTo);
        logger.debug("jsonString:" + jsonString);
        
        OutputStreamWriter out = new OutputStreamWriter(httpConn.getOutputStream());
        out.write(jsonString);
        out.close();
        
        int respCode = httpConn.getResponseCode();
        String msg = httpConn.getResponseMessage();
        logger.debug("respcode:" + respCode);
        logger.debug("message:" + msg);
        try
        {
            ErrorHandler.checkResponse(respCode, msg);
        }
        catch(Exception e)
        {
            throw e;
        }
        
        JsonNode rootNode = mapper.readTree(httpConn.getInputStream());
        logger.debug("Rootresponse:" + rootNode.toString());
        JsonNode orgNode = rootNode.get("requests");
        RequestTO[] respTos = null;
        if (orgNode == null)
        {
            RequestTO org = mapper.treeToValue(rootNode, RequestTO.class);
            respTos = new RequestTO[1];
            respTos[0] = org;
        }
        else
        {
            respTos = mapper.treeToValue(orgNode, RequestTO[].class);
        }
        if (respTos.length != 1)
        {
            logger.error("ERROR createRequest processing response");
            throw new Exception("ERROR createRequest processing response");
        }
        
        RequestTO respReq = respTos[0];
        respReq.getStatus();
        logger.debug("Request Response:" + respReq.getStatus());
        httpConn.disconnect();
        return respReq.getStatus();
    }
    
    /**
     * create an OIM Request
     * @param justif - Justification for the request
     * @param beneficiaryId - Beneficiary ID. usr_key of the OIM user
     * @param entityId - Entity Key
     * @param entityType - Entity Type
     * @return
     * @throws Exception 
     */
    public RequestOperationStatus createCustomRequest(String justif,String beneficiaryId,String entityId,String entityType) throws Exception 
    {
        HttpURLConnection httpConn = this.getPostConnection(this.customRequestEndPoint);
        
        RequestsTO reqsTo = new RequestsTO();
        RequestTO reqTo = new RequestTO();
        UserTO beneficiary = new UserTO();
        List<UserTO> beneficiaries = new ArrayList();
        beneficiary.setId(beneficiaryId);
        beneficiaries.add(beneficiary);
        List<RequestTO> reqTos = new ArrayList();
        List<TargetEntity> entities = new ArrayList();
        TargetEntity ta1 = new TargetEntity();
        reqTo.setReqJustification(justif);
        //reqTo.setReqBeneficiary(beneficiaryId);
        //reqTo.setRequestId("99"+beneficiaryId);
        reqTo.setReqBeneficiaryList(beneficiaries);
        ta1.setEntityId(entityId);
        ta1.setEntityType(entityType);
        entities.add(ta1);
        reqTo.setReqTargetEntities(entities);
        
        reqTos.add(reqTo);
        reqsTo.setRequests(reqTos);
        
        ObjectMapper mapper = this.getObjectMapper();
        String jsonString = mapper.writeValueAsString(reqTo);
        logger.debug("jsonString:" + jsonString);
        
        OutputStreamWriter out = new OutputStreamWriter(httpConn.getOutputStream());
        out.write(jsonString);
        out.close();
        
        int respCode = httpConn.getResponseCode();
        String msg = httpConn.getResponseMessage();
        logger.debug("respcode:" + respCode);
        logger.debug("message:" + msg);
        try
        {
            ErrorHandler.checkResponse(respCode, msg);
        }
        catch(Exception e)
        {
            throw e;
        }
        
        JsonNode rootNode = mapper.readTree(httpConn.getInputStream());
        logger.debug("Rootresponse:" + rootNode.toString());
        RequestOperationStatus ros;
        if (rootNode != null)
        {
            ros = mapper.treeToValue(rootNode, RequestOperationStatus.class);
        }
        else
        {
            logger.error("ERROR createRequest processing response");
            throw new Exception("ERROR createRequest processing response");
        }
        
        httpConn.disconnect();
        if (ros == null)
        {
            logger.error("ERROR RequestOperationStatus response");
            throw new Exception("ERROR RequestOperationStatus response");
        }
        return ros;
    }
    
    /**
     * Create a role Request
     * @param userLogin - OIM usr_login of the Beneficiary
     * @param roleName - Name of the OIM role to request for the beneficiary
     * @param justification - justification for the request
     * @return Request Status
     * @throws Exception 
     */
    public String submitAssignRoleRequest(String userLogin, String roleName, String justification) throws Exception
    {
        HttpURLConnection httpConn = this.getPostConnection(this.reqEndPoint);
        
        if (userLogin == null || roleName == null || justification == null)
        {
            throw new Exception("Invalid Parms");
        }
        Map userMap = this.getUserByIdOrLogin(true, userLogin);
        if (userMap == null || userMap.isEmpty())
        {
            throw new Exception("Invalid User");
        }
        String beneficiaryId = (String)userMap.get("ID");
        logger.debug("Beneficiary:" + beneficiaryId);
        
        RequestsTO reqsTo = new RequestsTO();
        RequestTO reqTo = new RequestTO();
        List<RequestTO> reqTos = new ArrayList();
        List<TargetEntity> entities = new ArrayList();
        TargetEntity ta1 = new TargetEntity();
        reqTo.setReqJustification(justification);
        
        UserTO beneficiary = new UserTO();
        List<UserTO> beneficiaries = new ArrayList();
        beneficiary.setId(beneficiaryId);
        beneficiaries.add(beneficiary);
        reqTo.setReqBeneficiaryList(beneficiaries);
        
        Map roleMap = this.getRoleByName(roleName);
        if (roleMap == null || roleMap.isEmpty())
        {
            throw new Exception("Invalid Role");
        }
        String entityId = (String)roleMap.get("ID");
        ta1.setEntityId(entityId);
        ta1.setEntityType("Role");
        entities.add(ta1);
        reqTo.setReqTargetEntities(entities);
        
        reqTos.add(reqTo);
        reqsTo.setRequests(reqTos);
        
        ObjectMapper mapper = this.getObjectMapper();
        String jsonString = mapper.writeValueAsString(reqsTo);
        logger.debug("jsonString:" + jsonString);
        
        OutputStreamWriter out = new OutputStreamWriter(httpConn.getOutputStream());
        out.write(jsonString);
        out.close();
        
        int respCode = httpConn.getResponseCode();
        String msg = httpConn.getResponseMessage();
        logger.debug("respcode:" + respCode);
        logger.debug("message:" + msg);
        try
        {
            ErrorHandler.checkResponse(respCode, msg);
        }
        catch(Exception e)
        {
            throw e;
        }
        
        JsonNode rootNode = mapper.readTree(httpConn.getInputStream());
        JsonNode orgNode = rootNode.get("requests");
        RequestTO[] respTos = null;
        if (orgNode == null)
        {
            RequestTO org = mapper.treeToValue(rootNode, RequestTO.class);
            respTos = new RequestTO[1];
            respTos[0] = org;
        }
        else
        {
            respTos = mapper.treeToValue(orgNode, RequestTO[].class);
        }
        if (respTos.length != 1)
        {
            logger.error("ERROR createRequest processing response");
            throw new Exception("ERROR createRequest processing response");
        }
        
        RequestTO respReq = respTos[0];
        respReq.getStatus();
        logger.debug("Request Response:" + respReq);
        httpConn.disconnect();
        return respReq.getStatus();
        
    }
    
    /**
     * Create a role Request
     * @param userLogin - OIM usr_login of the Beneficiary
     * @param roleName - Name of the OIM role to request for the beneficiary
     * @param justification - justification for the request
     * @return Request Status
     * @throws Exception 
     */
    public RequestOperationStatus submitCustomAssignRoleRequest(String userLogin, String roleName, String justification) throws Exception
    {
        HttpURLConnection httpConn = this.getPostConnection(this.customRequestEndPoint);
        
        if (userLogin == null || roleName == null || justification == null)
        {
            throw new Exception("Invalid Parms");
        }
        Map userMap = this.getUserByIdOrLogin(true, userLogin);
        if (userMap == null || userMap.isEmpty())
        {
            throw new Exception("Invalid User");
        }
        String beneficiaryId = (String)userMap.get("ID");
        logger.debug("Beneficiary:" + beneficiaryId);
        
        RequestsTO reqsTo = new RequestsTO();
        RequestTO reqTo = new RequestTO();
        List<RequestTO> reqTos = new ArrayList();
        List<TargetEntity> entities = new ArrayList();
        TargetEntity ta1 = new TargetEntity();
        reqTo.setReqJustification(justification);
        
        UserTO beneficiary = new UserTO();
        List<UserTO> beneficiaries = new ArrayList();
        beneficiary.setId(beneficiaryId);
        beneficiaries.add(beneficiary);
        reqTo.setReqBeneficiaryList(beneficiaries);
        
        Map roleMap = this.getRoleByName(roleName);
        if (roleMap == null || roleMap.isEmpty())
        {
            throw new Exception("Invalid Role");
        }
        String entityId = (String)roleMap.get("ID");
        ta1.setEntityId(entityId);
        ta1.setEntityType("Role");
        entities.add(ta1);
        reqTo.setReqTargetEntities(entities);
        
        //reqTos.add(reqTo);
        //reqsTo.setRequests(reqTos);
        
        ObjectMapper mapper = this.getObjectMapper();
        String jsonString = mapper.writeValueAsString(reqTo);
        logger.debug("jsonString:" + jsonString);
        
        OutputStreamWriter out = new OutputStreamWriter(httpConn.getOutputStream());
        out.write(jsonString);
        out.close();
        
        int respCode = httpConn.getResponseCode();
        String msg = httpConn.getResponseMessage();
        logger.debug("respcode:" + respCode);
        logger.debug("message:" + msg);
        JsonNode rootNode = mapper.readTree(httpConn.getInputStream());
        logger.debug("Rootnode:" + rootNode);
        httpConn.disconnect();
        
        //JsonNode rootNode = mapper.readTree(httpConn.getInputStream());
        RequestOperationStatus ros;
        if (rootNode != null)
        {
            ros = mapper.treeToValue(rootNode, RequestOperationStatus.class);
            return ros;
        }
        
        try
        {
            ErrorHandler.checkResponse(respCode, msg);
        }
        catch(Exception e)
        {
            throw e;
        }
        throw new Exception("Invalid Response");
    }
    
    /**
     * get an HTTP Connection suitable for a PUT operation
     * @param urlStr - URL of the OIM Rest Service
     * @return - HttpURLConnection
     * @throws Exception 
     */
    private HttpURLConnection getPutConnection(String urlStr) throws Exception
    {
        String newUrl = this.setUrl(urlStr);
        URL url = new URL(newUrl);
        HttpURLConnection httpConn = (HttpURLConnection)url.openConnection();
        httpConn.setRequestMethod("PUT");
        httpConn.setRequestProperty("Content-Type", "application/json");
        httpConn.setRequestProperty("Accept", "application/json");
        String authHeaderValue = "Basic " + new String(authString);
        httpConn.setRequestProperty("Authorization", authHeaderValue);
        httpConn.setDoOutput(true);
        return httpConn;
        
    }
    
    /**
     * get an HTTP Connection suitable for a POST operation
     * @param urlStr - URL of the OIM Rest Service
     * @return HttpURLConnection
     * @throws Exception 
     */
    private HttpURLConnection getPostConnection(String urlStr) throws Exception
    {
        String newUrl = this.setUrl(urlStr);
        URL url = new URL(newUrl);
        HttpURLConnection httpConn = (HttpURLConnection)url.openConnection();
        httpConn.setRequestMethod("POST");
        httpConn.setRequestProperty("Content-Type", "application/json");
        httpConn.setRequestProperty("Accept", "application/json");
        String authHeaderValue = "Basic " + this.authString;
        httpConn.setRequestProperty("Authorization", authHeaderValue);
        httpConn.setDoOutput(true);
        return httpConn;
    }
    
    /**
     * get an HTTP Connection suitable for a GET operation
     * @param urlStr - URL of the OIM Rest Service
     * @return HttpURLConnection
     * @throws Exception 
     */
    private HttpURLConnection getGetConnection(String urlStr) throws Exception
    {
        String newUrl = this.setUrl(urlStr);
        URL url = new URL(newUrl);
        HttpURLConnection httpConn = (HttpURLConnection)url.openConnection();
        httpConn.setRequestMethod("GET");
        httpConn.setRequestProperty("Accept", "application/json");
        String authHeaderValue = "Basic " + new String(this.authString);
        httpConn.setRequestProperty("Authorization", authHeaderValue);
        return httpConn;
    }
    
    /**
     * get a Jackson ObjectMapper
     * @return 
     */
    private ObjectMapper getObjectMapper()
    {
        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.setSerializationInclusion(Include.NON_NULL);
        return mapper;
    }
    
    /**
     * replace the template tag with the actual OIM host and port.
     * @param url - URL of OIM Host and Port.
     * @return 
     */
    private String setUrl(String url)
    {
        logger.debug("oldurl:" + url);
        String newUrl = url.replace("<HOSTNAME>",this.hostName);
        logger.debug("newurl:" + newUrl);
        return newUrl;
    }
    
}
