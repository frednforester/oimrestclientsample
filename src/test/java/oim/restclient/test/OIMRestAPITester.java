/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oim.restclient.test;

import oim.restclient.OIMRestAPI;
import oim.restclient.tos.RequestOperationStatus;
import org.junit.Test;

/**
 *
 * @author Owner
 */
public class OIMRestAPITester {
    
    private String user = "xelsysadm";
    private String pw = "ZZzz9191";
    private String url = "http://oim.testserver.org:14000";
    @Test
    public void mainTest()
    {
        OIMRestAPI restApi;
        try
        {
            restApi = new OIMRestAPI(user,pw,url);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return;
        }
        
        try
        {
            //RequestOperationStatus status = restApi.createCustomRequest("Requesting for application", "105", "3", "Appinstance");
            //System.out.println("ros:" + status);
            RequestOperationStatus status = restApi.submitCustomAssignRoleRequest("GD10055","PDAUser","Gotta test it");
            /*
            Map orgMap = restApi.getOrganizationByName("NBIS");
            if (orgMap != null)
            {
                System.out.println("Org:" + orgMap.get("Organization Name"));
                System.out.println("Org:" + orgMap);
            }
            
            Map userMap = restApi.getUserByIdOrLogin(true,"AP10062");
            if (userMap != null)
            {
                System.out.println("User:" + userMap.get("User Login"));
                System.out.println("User:" + userMap); 
            }
            Map roleMap = restApi.getRoleByName("DMYUser");
            if (roleMap != null)
            {
                System.out.println("Role:" + roleMap.get("Role Name"));
                System.out.println("Role:" + roleMap); 
            }
            roleMap = restApi.getRoleByRoleName("ALL USERS");
            if (roleMap != null)
            {
                System.out.println("Role:" + roleMap.get("Role Name"));
                System.out.println("Role:" + roleMap); 
            }
            ApplicationTO app = restApi.getApplicationByName("DMYApplication");
            if (app != null)
            {
                System.out.println("App:" + app.getName());
                System.out.println("App:" + app); 
            }
            if (app != null)
            {
                EntitlementTO ent = restApi.getEntitlementByNameObj("ADMIN_DMY",app.getObjectKey());
                System.out.println("Ent:" + ent.getName());
                System.out.println("Ent:" + ent); 
            }
            */
            /*
            String userLogin = restApi.findUserLoginByCN("UDI.JANSCH.69");
            System.out.println("UserLogin:" + userLogin); 
            userMap = restApi.getUserByIdOrLogin(true, userLogin);
            System.out.println("actkey:" + userMap.get("act_key")); 
            restApi.assignOrganizationToUser(userLogin,"NBIS");
            Map updMap = new HashMap();
            updMap.put("First Name","Freddy");
            restApi.updateUserByIdUsrKey((String)userMap.get("ID"), updMap);
            userMap = restApi.getUserByIdOrLogin(true, userLogin);
            System.out.println("actkey:" + userMap.get("act_key"));
            restApi.setUserOrgStatus(userLogin, "Approved");
            
            restApi.submitAssignRoleRequest("GD10055","PDAUser","Gotta test it");
            restApi.createRequest("Requesting for application", "105", "3", "Appinstance");
            restApi.createCustomRequest("Requesting for application", "105", "3", "Appinstance");
            List<String> userRoles = restApi.getUserRoles("BB12197");
            System.out.println("userroles:" + userRoles);
            userRoles = restApi.getUserRolesByCatalogCategory("Custom Role","BB12197");
            System.out.println("userroles:" + userRoles);
            */

        }
        catch(Exception e)
        {
            System.out.println("ERROR:" + e.getMessage());
            e.printStackTrace();
        }
    }
    
}
