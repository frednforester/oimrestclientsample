/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package oim.restclient.test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import oim.restclient.ErrorHandler;
import org.apache.commons.codec.binary.Base64;
import org.junit.Test;



/**
 *
 * @author Owner
 */
public class OIMRestClientTester {  
    
    
    String customEndPoint = "http://oim.testserver.org:14000/iam/governance/selfservice/api/v1/customroles";
    String lkupEndPoint = "http://oim.testserver.org:14000/iam/governance/selfservice/api/v1/customlookups";
    String rqstEndPoint = "http://oim.testserver.org:14000/iam/governance/selfservice/api/v1/customrequest";
    

    String authString = "xelsysadm:ZZzz9191";
    @Test
    public void mainTest() throws Exception 
    {
        String url = this.customEndPoint + "?q=usrKey eq 55";
        System.out.println("Calling:" + url);
        this.mainPrintJson(url);
        url = this.customEndPoint + "/memberships/2202";
        System.out.println("Calling:" + url);
        this.mainPrintJson(url);
        url = this.customEndPoint + "/categories/userkey/2202/Custom Role";
        System.out.println("Calling:" + url);
        this.mainPrintJson(url);
        url = this.customEndPoint + "/categories/userlogin/BB12197/Custom Role";
        System.out.println("Calling:" + url);
        this.mainPrintJson(url);
        url = this.lkupEndPoint + "?q=lookupName eq Lookup.USR_PROCESS_TRIGGERS";
        System.out.println("Calling:" + url);
        this.mainPrintJson(url);
        url = this.lkupEndPoint + "/name/Lookup.USR_PROCESS_TRIGGERS";
        System.out.println("Calling:" + url);
        this.mainPrintJson(url);
        
        url = this.lkupEndPoint + "/name/Lookup.USR_PROCESS_TRIGGERS";
        System.out.println("Calling:" + url);
        this.mainPrintJson(url);
        
        
        url = this.rqstEndPoint + "/name/" + "ALL USERS";
        System.out.println("Calling:" + url);
        this.mainPrintJson(url);
        
        if (1 == 1)
            return;
        
    }
    
  
     public void mainPrintJson(String url) throws Exception
    {
        URL hurl = new URL(url);
        Base64 encoder = new Base64();
        String authEnc = encoder.encodeToString(this.authString.getBytes());
        HttpURLConnection httpConn = (HttpURLConnection)hurl.openConnection();
        httpConn.setRequestMethod("GET");
        httpConn.setRequestProperty("Accept", "application/json");
        String authHeaderValue = "Basic " + new String(authEnc);
        httpConn.setRequestProperty("Authorization", authHeaderValue);
        
        int respCode = httpConn.getResponseCode();
        String msg = httpConn.getResponseMessage();
        System.out.println("respcode:" + respCode);
        System.out.println("message:" + msg);
        
        if (respCode != 200)
        {
            String error = ErrorHandler.getErrorMessage(respCode, msg);
            if (error != null)
            {
                System.out.println("Error:" + error);
                return;
            }
        }
        
        String output = null;
        BufferedReader bfr = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
        while((output = bfr.readLine()) != null)
        {
            System.out.println(output);
        }
        
    }
     
    private HttpURLConnection getPostConnection(String urlStr) throws Exception
    {
        URL url = new URL(urlStr);
        HttpURLConnection httpConn = (HttpURLConnection)url.openConnection();
        httpConn.setRequestMethod("POST");
        httpConn.setRequestProperty("Content-Type", "application/json");
        httpConn.setRequestProperty("Accept", "application/json");
        String authHeaderValue = "Basic " + this.authString;
        httpConn.setRequestProperty("Authorization", authHeaderValue);
        httpConn.setDoOutput(true);
        return httpConn;
    }
    
}
